# API de Prueba con la tematica de una veterinaria
![Logo](https://www.petsclinic.org/wp-content/uploads/2020/01/LOGO-WITH-STROKE-OUTLINE.png)

```Expone las apis con metodos GET:``` <br/>
1. [http:localhost:8080/api/pets/cats](http:localhost:8080/api/pets/cats)
1. [http:localhost:8080/api/pets/info](http:localhost:8080/api/pets/info)

```Su estructura es simple, se divide en 3 capas modelo y acceso de datos, negocio y rest services```

> Author: ***Josue David Reyes Molina***
