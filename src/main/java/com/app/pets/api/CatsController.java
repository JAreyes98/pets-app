package com.app.pets.api;

import com.app.pets.business.service.BusinessService;
import com.app.pets.business.service.PetService;
import com.app.pets.model.pojos.Pet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/pets")
public class CatsController {

    private final PetService petService;
    private final BusinessService businessService;

    @Autowired
    public CatsController(final PetService petService, BusinessService businessService) {
        this.petService = petService;
        this.businessService = businessService;
    }

    @GetMapping("/cats")
    public ResponseEntity<?> getAll(){
        Gson gson = new GsonBuilder().create();
        return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(petService.getCats()));
    }

    @GetMapping("/info")
    public ResponseEntity<?> getInfo(){
        Gson gson = new GsonBuilder().create();
        return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(businessService.findById(1).orElse(null)));
    }
}
