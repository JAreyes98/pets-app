package com.app.pets.business.service;

import com.app.pets.model.dao.PetsDao;
import com.app.pets.model.pojos.KindPet;
import com.app.pets.model.pojos.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
public class PetService {

    private final PetsDao petsDao;

    @Autowired
    public PetService(final PetsDao petsDao) {
        this.petsDao = petsDao;
    }

    public List<Pet> getCats() {
        Predicate<Pet> predicate = pet -> pet.getKindPet() == KindPet.CAT;
        return petsDao.getAllIf(predicate);
    }

    public List<Pet> getFish() {
        Predicate<Pet> predicate = pet -> pet.getKindPet() == KindPet.FISH;
        return petsDao.getAllIf(predicate);
    }

    public List<Pet> getDogs() {
        Predicate<Pet> predicate = pet -> pet.getKindPet() == KindPet.DOG;
        return petsDao.getAllIf(predicate);
    }
}
