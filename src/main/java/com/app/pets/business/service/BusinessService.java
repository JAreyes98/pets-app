package com.app.pets.business.service;

import com.app.pets.model.dao.BusinessDao;
import com.app.pets.model.pojos.Business;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BusinessService implements BusinessDao {
    @Override
    public List<Business> getBusiness() {
        return List.of(
                new Business(1,"Pet clinic S.A", "San rafael", "1234")
                ,new Business(2,"Pet clinic 2 S.A", "San Sebastian", "1234")
        );
    }

    public Optional<Business> findById(Integer id) {
        return getBusiness().stream().filter(f -> f.getId() == id).findFirst();
    }
}
