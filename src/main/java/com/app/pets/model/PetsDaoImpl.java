package com.app.pets.model;

import com.app.pets.model.dao.PetsDao;
import com.app.pets.model.pojos.KindPet;
import com.app.pets.model.pojos.Pet;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Repository
public class PetsDaoImpl implements PetsDao {
    @Override
    public List<Pet> getAll() {
        return Arrays.asList(
                new Pet("Lulu", (short) 3, KindPet.DOG),
                new Pet("MARTHA", (short) 3, KindPet.CAT),
                new Pet("BOBY", (short) 3, KindPet.DOG),
                new Pet("NEMO", (short) 3, KindPet.FISH),
                new Pet("KEVIN", (short) 3, KindPet.DOG),
                new Pet("SIMON", (short) 3, KindPet.CAT),
                new Pet("LUIS", (short) 3, KindPet.DOG),
                new Pet("ALEX", (short) 3, KindPet.DOG),
                new Pet("TIMMY", (short) 3, KindPet.FISH),
                new Pet("CARLA", (short) 3, KindPet.FISH),
                new Pet("STEVEN", (short) 3, KindPet.DOG),
                new Pet("GATO CON BOTAS", (short) 3, KindPet.CAT),
                new Pet("RONAL", (short) 3, KindPet.DOG)
        );
    }

    @Override
    public List<Pet> getAllIf(Predicate<Pet> predicate) {
        return this.getAll().stream().filter(predicate).collect(Collectors.toList());
    }
}
