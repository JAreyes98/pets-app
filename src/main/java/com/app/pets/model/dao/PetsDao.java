package com.app.pets.model.dao;

import com.app.pets.model.pojos.Pet;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Predicate;

public interface PetsDao {
    List<Pet> getAll();
    List<Pet> getAllIf(Predicate<Pet> predicate);
}
