package com.app.pets.model.dao;

import com.app.pets.model.pojos.Business;

import java.util.List;
import java.util.Optional;

public interface BusinessDao {
    List<Business> getBusiness();
    Optional<Business> findById(Integer id);
}
