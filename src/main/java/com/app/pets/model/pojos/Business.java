package com.app.pets.model.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Business {
    private int id;
    private String name;
    private String address;
    private String ruc;
}
