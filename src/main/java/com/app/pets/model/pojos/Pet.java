package com.app.pets.model.pojos;

import lombok.Data;

@Data
public class Pet {
    private String name;
    private Short age;
    private KindPet kindPet;

    public Pet(String name, Short age, KindPet kindPet) {
        this.name = name;
        this.age = age;
        this.kindPet = kindPet;
    }
}
