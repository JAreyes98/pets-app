package com.app.pets.model.pojos;

public enum KindPet {
    DOG, CAT, FISH
}
